package com.example.huellas;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import pl.droidsonroids.gif.GifImageButton;
import com.example.huellas.FormCedula;

public class MainActivity extends AppCompatActivity {

    private TextView resultTExtView;
    private GifImageButton btn_start;
    private Button scan_btn;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        resultTExtView=findViewById(R.id.result_text);
        scan_btn=findViewById(R.id.btn_scan);
        scan_btn.setOnClickListener(monclicklister);
        btn_start=(GifImageButton)findViewById(R.id.btn_start);
        btn_start.setOnClickListener(monclicklister);

        context = getApplicationContext();

    }
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){

        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result= IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null)
            if(result.getContents() != null){
                Toast.makeText(MainActivity.this,"El codigo es:"+ result.getContents(), Toast.LENGTH_SHORT).show();
                //resultTExtView.setText("El codigo es:"+ result.getContents());
            }else{
                //resultTExtView.setText("Error al escanear el codigo Qr");
                Toast.makeText(MainActivity.this,"Error al escanear el codigo Qr", Toast.LENGTH_SHORT).show();
            }
    }
    private View.OnClickListener monclicklister=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch(view.getId()){
                case R.id.btn_scan:
                    new IntentIntegrator(MainActivity.this).initiateScan();
                    break;
                case R.id.btn_start:
                    Intent intens = new Intent(context,FormCedula.class);
                    context.startActivity(intens);
                    break;
            }
        }
    };


}
